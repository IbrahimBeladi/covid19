import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
	theme: {
		themes: {
			light: {
				primary: "#5EA461",
			},
			dark: {
				primary: "#5EA461",
			}
		},
		dark: true
	}
});
