module.exports = {
	configureWebpack: {
		resolve: {
			alias: {
				"@": "/home/ibrahim/Web/covid19/src"
			}
		}
	},
	transpileDependencies: [
		"vuetify"
	],
	pluginOptions: {
		i18n: {
			locale: 'en',
			fallbackLocale: 'en',
			localeDir: 'locales',
			enableInSFC: false
		}
	},
	chainWebpack: config => {
		config
			.plugin('html')
			.tap(args => {
				args[0].title = 'COVID19'
				return args
			})
	},
}
